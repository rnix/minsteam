<?php

return [
    'mailFrom' => [
        getenv('MAIL_FROM_EMAIL') ? getenv('MAIL_FROM_EMAIL') : getenv('MAIL_USERNAME')
        => getenv('MAIL_FROM_NAME') ? getenv('MAIL_FROM_NAME') : 'Min Steam'
    ]
];
