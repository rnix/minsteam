<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => getenv('DB_DSN') ? getenv('DB_DSN') : 'mysql:host=localhost;dbname=minsteam',
    'username' => getenv('DB_USERNAME') ? getenv('DB_USERNAME') : 'root',
    'password' => getenv('DB_PASSWORD') ? getenv('DB_PASSWORD') : '',
    'charset' => 'utf8',
];
