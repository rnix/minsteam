<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $email
 * @property string $ins_date
 * @property string $activation_date
 * @property string $unsubscribe_date
 * @property string $action_code
 *
 * @property Users2Games[] $users2games
 */
class User extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email'], 'required'],
            [['email'], 'string', 'max' => 128],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'ins_date' => 'Ins Date',
            'activation_date' => 'Activation Date',
            'unsubscribe_date' => 'Unsubscribe Date',
            'action_code' => 'Action Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers2Games() {
        return $this->hasMany(Users2Games::className(), ['user_id' => 'id']);
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            
            if ($insert) {
                $this->action_code = md5(uniqid('', true));
            }
            
            return true;
        } else {
            return false;
        }
    }

    public function sendActivation() {
        \Yii::$app->mail->compose('activation', ['user' => $this,])
                ->setTo($this->email)
                ->setFrom(\Yii::$app->params['mailFrom'])
                ->setSubject('Confirm your email')
                ->send();
    }

}
