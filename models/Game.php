<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "games".
 *
 * @property integer $steam_id
 * @property string $title
 * @property string $url
 * @property double $price
 * @property string $ins_date
 * @property string $last_date
 *
 * @property Users2Games[] $users2games
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['steam_id'], 'required'],
            [['steam_id'], 'integer'],
            [['title', 'url'], 'string'],
            [['price'], 'number'],
            [['ins_date', 'last_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'steam_id' => 'Steam ID',
            'title' => 'Title',
            'url' => 'Url',
            'price' => 'Price',
            'ins_date' => 'Ins Date',
            'last_date' => 'Last Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers2Games()
    {
        return $this->hasMany(Users2Games::className(), ['game_id' => 'steam_id']);
    }
}
