<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users2games".
 *
 * @property integer $user_id
 * @property integer $game_id
 * @property string $ins_date
 *
 */
class Users2Games extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users2games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'game_id'], 'required'],
            [['game_id'], 'integer'],
            [['ins_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'game_id' => 'Game ID',
            'ins_date' => 'Ins Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['steam_id' => 'game_id']);
    }
    
    public static function matchNewGames($games) {
        $newUsers2Games = array();
        foreach ($games as $game) {
            $usersToNotify = User::find()
                    ->select('users.*')
                    ->leftJoin('users2games', '`users2games`.`user_id` = `users`.`id` AND `users2games`.`game_id` = :game_id', [':game_id' => $game->steam_id])
                    ->where(['`users2games`.`user_id`' => null, '`users`.`unsubscribe_date`' => null])
                    ->andWhere(['not', ['users.activation_date' => null]])
                    ->all();

            foreach ($usersToNotify as $user) {
                $u2g = new Users2Games;
                $u2g->game_id = $game->steam_id;
                $u2g->user_id = $user->id;
                $u2g->save();
                $newUsers2Games[] = $u2g;
            }
        }
        return $newUsers2Games;
    }

}
