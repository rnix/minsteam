<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

Follow the link to confirm your email address:<br>
<?php $url = Url::toRoute(['/site/activate', 'email' => urlencode($user->email), 'hash' => $user->action_code]); ?>
<a href="<?= $url ?>"><?= $url ?></a>

<br>
<br>
If it was not you, <a href="<?= Url::toRoute(['/site/delete', 'email' => urlencode($user->email), 'hash' => $user->action_code]); ?>">let us know</a>.
