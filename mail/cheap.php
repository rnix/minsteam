<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<a href="http://store.steampowered.com/search/?cc=ru&specials=1&os=win&sort_by=Price_ASC&category2=29&page=1">Список дешёвых</a> игр в Steam:

<ul>
    <?php foreach ($games as $game) { ?>
        <li><a href="<?=  Html::encode($game->url) ?>"><?= Html::encode($game->title) ?> </a>: <?= $game->price ?>р.</li> 
    <?php } ?>
</ul>

<br>
<br>
<small>
    <a href="<?= Url::toRoute(['/site/unsubscribe', 'email' => urlencode($user->email), 'hash' => $user->action_code]); ?>">Unsubscribe</a>
</small>
