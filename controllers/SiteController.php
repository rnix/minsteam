<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;

class SiteController extends Controller {

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->sendActivation();
            Yii::$app->session->setFlash('subscribeFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('index', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUnsubscribe($email, $hash) {
        $email = urldecode($email);
        $user = User::find()->where(['email' => $email])->one();
        if (!$user) {
            return;
        }
        if (!$user->unsubscribe_date && $user->action_code === $hash) {
            $user->unsubscribe_date = new yii\db\Expression('NOW()');
            $user->save();
            Yii::$app->session->setFlash('unsubscribeFormSubmitted');
            return $this->refresh();
        }
        return $this->render('unsubscribe', ['user' => $user]);
    }
    
    public function actionActivate($email, $hash) {
        $email = urldecode($email);
        $user = User::find()->where(['email' => $email])->one();
        if ($user && !$user->activation_date && $user->action_code === $hash) {
            $user->activation_date = new yii\db\Expression('NOW()');
            $user->save();
            Yii::$app->session->setFlash('activateFormSubmitted');
            return $this->refresh();
        }
        return $this->render('activate', ['user' => $user]);
    }
    
    public function actionDelete($email, $hash) {
        $email = urldecode($email);
        $user = User::find()->where(['email' => $email])->one();
        if ($user && !$user->activation_date && $user->action_code === $hash) {
            $user->delete();
            Yii::$app->session->setFlash('deleteFormSubmitted');
            return $this->refresh();
        }
        return $this->render('delete', ['user' => $user]);
    }

}
