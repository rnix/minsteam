<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Game;
use app\models\User;
use app\models\Users2Games;

class CheckController extends Controller {

    public function actionCheap() {
        set_time_limit(120);
        $maxPrice = 7;
        $client = new \Goutte\Client();
        $client->setHeader('User-Agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0');

        try {
            $crawler = $client->request('GET', 'http://store.steampowered.com/search/?sort_by=Price_ASC&category1=998&category2=29&os=win&specials=1');

            $items = array();
            $crawler->filter('#search_result_container a.search_result_row')->each(function ($node) use (&$items, $maxPrice) {
                try {
                    $item = array();
                    $id = intval($node->attr('data-ds-appid'));
                    $item['id'] = $id;
                    $item['url'] = $node->link()->getUri();
                    $item['title'] = $node->filter('.title')->first()->text();
                    $priceNode = $node->filter('.search_price')->first();
                    $priceRaw = $priceNode->filterXPath('//div/text()[2]')->first()->text();
                    $price = intval(preg_replace('/[^\d]/', '', $priceRaw));
                    $item['price'] = $price;
                    if ($price <= $maxPrice) {
                        $items[] = $item;
                    }
                } catch (Exception $ex) {
                    trigger_error($ex->getMessage());
                }
            });
        } catch (Exception $ex) {
            trigger_error('Crawler error: ' . $ex->getMessage());
            return;
        }
        /* End of parsing */

        echo "Found items: " . count($items) . "\n";
        
        $games = $this->_saveItems($items);
        $users2games = Users2Games::matchNewGames($games);
        $this->_notifyUsers($users2games);
    }
    
    protected function _saveItems($items) {
        $savedModels = array();
        foreach ($items as $item) {
            $id = (int) $item['id'];
            $game = Game::findOne($id);
            if (!$game) {
                $game = new Game();
            }
            $game->steam_id = $id;
            $game->url = $item['url'];
            $game->title = $item['title'];
            $game->price = $item['price'];
            $game->last_date = new \yii\db\Expression('NOW()');
            $game->save();
            $savedModels[] = $game;
        }
        
        return $savedModels;
    }
    
    /**
     * 
     * @param Users2Games[] $users2games
     */
    protected function _notifyUsers($users2games) {
        $groupedByUsers = array();
        foreach ($users2games as $u2g) {
            if (!array_key_exists($u2g->user_id, $groupedByUsers)) {
                $groupedByUsers[$u2g->user_id] = [
                    'user' => $u2g->getUser()->one(),
                    'games' => array()];
            }
            $groupedByUsers[$u2g->user_id]['games'][] = $u2g->getGame()->one();
        }
        $i = 0;
        foreach ($groupedByUsers as $u2gData) {
            \Yii::$app->mail->compose('cheap', ['user' => $u2gData['user'], 'games' => $u2gData['games']])
                    ->setFrom(\Yii::$app->params['mailFrom'])
                    ->setTo($u2gData['user']->email)
                    ->setSubject('Новых дешёвых игр в Steam: ' . count($u2gData['games']))
                    ->send();
            $i++;
        }
        echo "Emails sent: $i\n";
    }

}
