<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Unsubscribe to the cheapest games on the Steam';
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('unsubscribeFormSubmitted')): ?>

    <div class="alert alert-success">
        You have been unsubscribed.
    </div>


    <?php else: ?>
    
    
        <?php if ($user->unsubscribe_date) { ?>
            <div class="alert alert-danger">
                This email is already unsubscribed.
            </div>
        <?php } ?>

    <div class="row">
        <div class="col-lg-5">
            Follow the link in your email if you want to unsubscribe.
        </div>
    </div>

    <?php endif; ?>
</div>
