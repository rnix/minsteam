<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Delete email from data base';
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('deleteFormSubmitted')): ?>

    <div class="alert alert-success">
        You email has been deleted.
    </div>


    <?php else: ?>
    
        <?php if (!$user) { ?>
            <div class="alert alert-danger">
                This email not found.
            </div>
        <?php } else if ($user->activation_date) { ?>
            <div class="alert alert-warning">
                This email is activated. You can unsubscribe if you follow the link in your email.
            </div>
        <?php } ?>

    <div class="row">
        <div class="col-lg-5">
            Follow the link in your email if you want to delete it.
        </div>
    </div>

    <?php endif; ?>
</div>
