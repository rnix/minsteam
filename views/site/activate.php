<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Activate the subscription to the cheapest games on the Steam';
?>
<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('activateFormSubmitted')): ?>

    <div class="alert alert-success">
        You email has been activated.
    </div>


    <?php else: ?>
    
        <?php if (!$user) { ?>
            <div class="alert alert-danger">
                This email not found.
            </div>
        <?php } else if ($user->activation_date) { ?>
            <div class="alert alert-warning">
                This email is already activated.
            </div>
        <?php } ?>

    <div class="row">
        <div class="col-lg-5">
            Follow the link in your email if you want to activate it.
        </div>
    </div>

    <?php endif; ?>
</div>
